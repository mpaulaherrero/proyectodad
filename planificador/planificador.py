from pymongo import MongoClient
from pprint import pprint
import datetime
import sys
import os
import requests
import threading
import time
from urllib.parse import urlparse
from urllib.parse import urldefrag
import re

def agregar_url_pila(urlPadre, nuevoUrl):
	print(threading.currentThread().getName(), ": url a agregar:" + nuevoUrl + " hijo de: " + urlPadre)
	urlPartes = urlparse(nuevoUrl)
	dominio = urlPartes.scheme + "://" + urlPartes.netloc
	#print('dominio', dominio)
	#averiguo si existe el domino
	existeDominio = db.dominios.find({"host":dominio})
	if existeDominio.count() <= 0:
		db.dominios.insert({"host": dominio})
		print(threading.currentThread().getName(), ": dominio nuevo insertar")
	#inserto url
	createdDate = datetime.datetime.utcnow()
	db.urls.insert({"url": nuevoUrl,"url_father": urlPadre,"created_date": createdDate ,"status": "waiting" })

def is_excluido_url(padre, url):
	#se excluyen los archivos con las extensiones de mas adelante, lo que diga javascript y lo que no pertenezca al dominio
	padrePartes = urlparse(padre)
	urlPartes = urlparse(url)
	resultado = True
	if padrePartes.netloc == urlPartes.netloc:
		extensions = ['.pdf', '.doc', '.docx', '.zip', '.xls', '.xlst', '.rar', '.bz2', '.jpg', '.ppt', '.png', 'javascript:', 'mailto:']
		prog = re.compile(r'({})$'.format('|'.join(re.escape(x) for x in extensions)))
		resultado = bool(prog.search(url))
	return resultado

def agregar_urls_pila(url, nuevosUrls):
	print(threading.currentThread().getName(), ": url a actualizar: '" + url['url'] + "', y agregando nuevos urls")
	#pprint(nuevosUrls)
	for nuevoUrl in nuevosUrls:
		#le quito al url el fragmento que viene luego de #
		urlClean, fragment = urldefrag(nuevoUrl)
		if not is_excluido_url(url['url'], urlClean):
			existeUrl = db.urls.find({"url":urlClean})
			if existeUrl.count() > 0:
				#no hago nada el url ya esta en la lista
				print(threading.currentThread().getName(), ": url repetido:" + urlClean)
			else:
				#lo inserto para bajar
				agregar_url_pila(url['url'], urlClean)
		else:
			print(threading.currentThread().getName(), ": url excluido: " + urlClean)

def get_url_descargador(descargador):
	link = ""
	if descargador['link']:
		link = descargador['link']
		
	descargadorHOST = os.getenv(link + "PORT_8080_TCP_ADDR")
	descargadorPORT = os.getenv(link + "PORT_8080_TCP_PORT")

	if not descargadorHOST:
		descargadorHOST = descargador['host']
	if not descargadorPORT:
		descargadorPORT = descargador['port'] 			
	resultado = "http://" + descargadorHOST + ":" + descargadorPORT + "/pagina/"
	return resultado
	
#ok, tengo dos descargadores y un url, llamo al descargador1, le paso el url
#me duermo lo que me diga de respuesta
#me levanto, pido respuesta
#si recibo, proceso los urls hijos y mando otra vez a buscar
#si no recibo, me vuelvo a dormir
def procesar_url_descargador(url, descargador):
	print(threading.currentThread().getName(), ': Empezando')
	serverName = descargador['name']
	#db.descargadores.update({"name": serverName},{"$set": {'status' : 'busy', 'url_last': url['url']}}, upsert=False)
	db.urls.update({"url": url['url']},{"$set": {'descargador' : serverName, 'status': 'inProgress'}}, upsert=False)
	llamada = get_url_descargador(descargador)
	print(threading.currentThread().getName(), ": url a procesar: '" + url['url'] + "', con el servidor: '" + llamada + "'")
	urlProcesado = False
	esperarMaximo = 5
	statusServer = 'free'
	while not urlProcesado:
		#Creamos la petición HTTP con GET:
		try:
			r = requests.get(llamada, params = {"url": url['url'] })
			#Imprimimos el resultado si el código de estado HTTP es 200 (OK):
			if r.status_code == 200:
				#pprint(r.json())
				respuesta = r.json()
				#si en la respuesta viene "mensaje_id":"RECIBIDO", dormir "waitSecondsTime":10 y volver a llamar al servidor
				if respuesta['mensaje_id'] == "RECIBIDO":
					#pprint("dormir y volver a llamar")
					sleepingTime = respuesta['waitSecondsTime']
					print(threading.currentThread().getName(), ': detenido durante sec: ' +  str(sleepingTime))
					time.sleep(sleepingTime)
				elif respuesta['mensaje_id'] == "PROCESANDO":
					#pprint("dormir y volver a llamar")
					sleepingTime = respuesta['waitSecondsTime']
					print(threading.currentThread().getName(), ': detenido durante sec: ' +  str(sleepingTime))
					esperarMaximo = esperarMaximo -1
					if esperarMaximo == 0:
						urlProcesado = True
						db.urls.update({"url": url['url']},{"$set": {'status': 'tiempoEsperaExedido'}}, upsert=False)
					else:	
						time.sleep(sleepingTime)
				elif respuesta['mensaje_id'] == "ERROR":
					print(threading.currentThread().getName(), ': ERROR Descargador: ' + respuesta['mensaje'])
					urlProcesado = True
					db.urls.update({"url": url['url']},{"$set": {'status': 'errorDescargador'}}, upsert=False)
				else:
					#print('respuesta: ' + respuesta['mensaje_id'] )
					#si en la respuesta viene "mensaje_id":"FINALIZADO" o 'mensaje_id': 'WARNING', con 'url_array', procear urls relacionados y liberar el servidor
					agregar_urls_pila(url, respuesta['url_array'])
					db.urls.update({"url": url['url']},{"$set": {'status': 'processed'}}, upsert=False)
					urlProcesado = True	
			else:
				print(threading.currentThread().getName(), ': ERROR Descargador status_code: ' + str(r.status_code))
				urlProcesado = True
				db.urls.update({"url": url['url']},{"$set": {'status': 'errorRequest'}}, upsert=False)
		except requests.exceptions.Timeout:
		    # Maybe set up for a retry, or continue in a retry loop
			print(threading.currentThread().getName(), ': ERROR Descargador problemas de conexion, errno: Timeout')
			urlProcesado = True
			statusServer = 'requests.exceptions.Timeout'
			#db.descargadores.update({"name": serverName},{"$set": {'status' : 'requests.exceptions', 'url_last': ''}}, upsert=False)
		except requests.exceptions.TooManyRedirects:
		    # Tell the user their URL was bad and try a different one
			print(threading.currentThread().getName(), ': ERROR Descargador problemas de conexion, errno: TooManyRedirects')
			urlProcesado = True
			statusServer = 'requests.exceptions.TooManyRedirects'
			#db.descargadores.update({"name": serverName},{"$set": {'status' : 'requests.exceptions', 'url_last': ''}}, upsert=False)
		except requests.exceptions.ConnectionError:
		    # Tell the user their URL was bad and try a different one
			print(threading.currentThread().getName(), ': ERROR Descargador problemas de conexion, errno: ConnectionError, Remote end closed connection without response')
			urlProcesado = True
			statusServer = 'requests.exceptions.ConnectionError'
			#db.descargadores.update({"name": serverName},{"$set": {'status' : 'requests.exceptions', 'url_last': ''}}, upsert=False)
		except requests.exceptions.RequestException as e:
			print(threading.currentThread().getName(), ': ERROR Descargador problemas de conexion: ', e )
			urlProcesado = True
			statusServer = 'requests.exceptions'
			#db.descargadores.update({"name": serverName},{"$set": {'status' : 'requests.exceptions', 'url_last': ''}}, upsert=False)	
	db.descargadores.update({"name": serverName},{"$set": {'status' : statusServer, 'url_last': ''}}, upsert=False)
	print(threading.currentThread().getName(), ': Terminado')

def procesar_url(url,i):
	print(str(i) + " | url a procesar: '" + url['url'] + "'")
	descargadorEncontrado=False
	while not descargadorEncontrado:
		descargadores = db.descargadores.find()
		for descargador in descargadores :
			if descargador['status'] == "free":
				descargadorEncontrado=True
				descargador['status'] = "busy"
				db.descargadores.update({"name": descargador['name']},{"$set": {'status' : 'busy', 'url_last': url['url']}}, upsert=False)
				#procesar_url_descargador(url, descargador)
				t = threading.Thread(target=procesar_url_descargador, name=str(i) + ' | procesar_url_' + descargador['name'],args=(url, descargador))
				t.start()
				break
		if not descargadorEncontrado:
			#dormir xsegundos y volver a preguntar
			print(str(i) + " | no se encontro descargador para procesar el url: '" + url['url'] + "', duermo 60 segundos")	
			time.sleep(60)	
		
#for a in os.environ:
#	print('Var: ', a, 'Value: ', os.getenv(a))
	
mongodbHOST = os.getenv("MONGO_PORT_27017_TCP_ADDR")
mongodbPORT = os.getenv("MONGO_PORT_27017_TCP_PORT")

if not mongodbHOST:
	mongodbHOST = "localhost"
if not mongodbPORT:
	mongodbPORT = "27017"
		
print("MONGO_PORT_27017_TCP_ADDR", mongodbHOST)
print("MONGO_PORT_27017_TCP_PORT", mongodbPORT)

# conectar a bd
#client = MongoClient("mongodb://localhost:27017");
client = MongoClient("mongodb://" + mongodbHOST + ":" + mongodbPORT)
db=client.planificadordb
#semillas
i=0
j=1000
termine = False
print("EMPEZANDO")		
while not termine:
	urls1 = db.urls.find({"status":"waiting"})
	print(str(j) + " | vuelta cantidad de urls semilla: " + str(urls1.count()))
	if urls1.count() > 0:
		for url in urls1:
			procesar_url(url,i+j)
			i=i+1
		print(str(j) + " | revise todos los urls y descargadores duermo 60 segundos")	
		time.sleep(60)
	else:
		if j > 3000:
			#valido hasta 5 veces a ver si realmente termine en los threads
			termine = True
			print(str(j) + " | No mas urls que procesar")		
		else:
			j=j+1000
			print(str(j) + " | revise todos los urls duermo 60 segundos")	
			time.sleep(60)		
print("TERMINANDO")	
