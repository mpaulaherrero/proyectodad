'use strict';
module.exports = function(app) {
  var descargador = require('../controllers/descargadorController');

  // todoList Routes
  app.route('/pagina')
    .get(descargador.read_a_page);
};