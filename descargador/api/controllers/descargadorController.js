'use strict';

var mongoose = require('mongoose'),
	Page = mongoose.model('Pages'),
	Config = mongoose.model('Config'),
	PythonShell = require('python-shell');

function update_server_status (newStatus, url) {
  global.statusProcess = newStatus;
  global.urlProcess = url;
  var query = {"_id": 1};
  var update = {
	 $set: {
		"status_process" : newStatus,
		"url_process" : url,
	}
  } 
  Config.findOneAndUpdate(query, update,  function(err, task) {
    if (err)
      	console.log("ERROR update_server_status", err);
	else 
		console.log("status_process modificado a '%s'", newStatus);
  });
};

function recibido(url){
	//mongodb busy
	update_server_status("busy",url);
	//llamo al thread de procesar
	console.log("invoco a pythone para procesar '%s'", url);
	var options = {
	    pythonPath: 'python3',
		args: [ url ]
	};
	PythonShell.run('descargador.py', options, function (err,results) {
	  	if (err) {
			console.log('ERROR llamando a phytone ', err);			
		} else {
			console.log('finished results: %j', results);
		}	
	});
}

function process_url(url,urlExiste, urls, res){
	var result = new Object();
	result ["url"] = url;
	console.log("urls:", urls);
	
	if(global.statusProcess == "free" && !urlExiste){
		console.log("FREE");
		result ["mensaje_id"] = "RECIBIDO";
		result ["mensaje"] = "URL recibido y comienza el procesamiento del mismo";
		result ["waitSecondsTime"] = global.waitSecondsTime;
		recibido(url);
	} else if(global.statusProcess == "busy"){
		console.log("BUSY");
		//if ( global.urlProcess == url){
			result ["mensaje_id"] = "PROCESANDO";
			result ["mensaje"] = "URL aun esta en proceso, seguir esperando";	
			//mando un tiempo de dormir menor	
			result ["waitSecondsTime"] = global.waitSecondsTime;
		//} else {
		//	result ["mensaje_id"] = "ERROR";
		//	result ["mensaje"] = "url no corresponde con el que se esta procesando";
		//	console.log("ERROR url diferente procesando '%s', nuevo '%s'", global.urlProcess, url);
		//}		
	} else if(global.statusProcess == "ready"){
		console.log("READY");
		if ( global.urlProcess == url){
			result ["mensaje_id"] = "FINALIZADO";
			result ["mensaje"] = "URL procesado, entregamos lista de url de la pagina";
			result ["url_array"] = urls;
			//coloco el servidor como liberado para que procese otro
			update_server_status("free","");
		} else {
			//result ["mensaje_id"] = "ERROR";
			//result ["mensaje"] = "url no corresponde con el que se esta procesando";
			//console.log("ERROR url diferente procesando '%s', nuevo '%s'", global.urlProcess, url);
			console.log("READY-NOTURL");
			result ["mensaje_id"] = "RECIBIDO";
			result ["mensaje"] = "URL recibido y comienza el procesamiento del mismo";
			result ["waitSecondsTime"] = global.waitSecondsTime;
			recibido(url);
		}
	}else if(global.statusProcess == "free" && urlExiste){
		result ["mensaje_id"] = "WARNING";
		result ["mensaje"] = "El url ya fue procesado";
		result ["url_array"] = urls;
	} else {
		result ["mensaje_id"] = "ERROR";
		result ["mensaje"] = "comunicarse con el desarrollador";
		console.log("ERROR status no conocido '%s'", global.statusProcess);
	}
	res.status(200).send(JSON.stringify(result)).end();
}

function search_related_urs(url,res){
	var cursor = Page.find({"url": url}).cursor();
	cursor.next(function(error, doc) {
		var urlExiste = false;
		var urls = "";
		if (doc !=null) {
			urlExiste = true;
			urls = doc.url_related;
		}
		process_url(url,urlExiste, urls,res);
	});			
}

exports.read_a_page = function(req, res) {
	var url = req.query.url;
	//leo status desde BD para ver si python lo cambio
	var cursor = Config.find({}, function(err, data) {
		global.statusProcess = data[0].status_process;
		console.log("read_a_page : status_process es '%s'",statusProcess);
		search_related_urs(url,res);
	});
};