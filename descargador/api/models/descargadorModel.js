'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ConfSchema = new Schema({
	_id:{
		type: Number
	},
	wait_seconds_time:{
		type: Number
	},
	status_process:{
		type: String,
	      enum: ['busy', 'ready', 'free']
	},
	url_process:{
		type: String
	}
});

var PageSchema = new Schema({
  url: {
    type: String,
    required: 'URL de la página'
  },
  created_date: {
    type: Date,
    default: Date.now
  },
  downdload_date: {
    type: Date,
    default: Date.now
  },
  status: {
    type: [{
      type: String,
      enum: ['stored', 'notStored']
    }],
    default: ['notStored']
  },
  status_browser: {
	type: String
  },
  url_related: {
    type: [{
      type: String
    }]
  },
  html: {
    type: String
  },
  text_dom: {
    type: String
  }
});

module.exports = mongoose.model('Config', ConfSchema);
module.exports = mongoose.model('Pages', PageSchema);