var express = require('express'),
  app 		= express(),
  port 		= process.env.PORT || 3000,
  mongoose 	= require('mongoose'),
  Task 		= require('./api/models/descargadorModel'), //created model loading here
  bodyParser = require('body-parser');

global.waitSecondsTime;
global.statusProcess;
global.url;

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/descargadordb'); 
mongoose.set('debug', true);

//find global info from DB
mongoose.model('Config').find({}, function(err, data) {
	//console.log(err,data, data.length);
	global.waitSecondsTime = data[0].wait_seconds_time;
	global.statusProcess = data[0].status_process;
	global.urlProcess = data[0].url_process;
	console.log("waitSecondsTime=%s",waitSecondsTime);
	console.log("statusProcesss=%s",statusProcess);
	console.log("urlProcesss='%s'",urlProcess);
  });

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./api/routes/descargadorRoutes'); //importing route
routes(app); //register the route

app.listen(port);

console.log('servidor del descargador RESTful API levantado en: ' + port);