from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from pymongo import MongoClient
from pprint import pprint
import datetime
import sys
import os

mongodbHOST = os.getenv("MONGO_PORT_27017_TCP_ADDR")
mongodbPORT = os.getenv("MONGO_PORT_27017_TCP_PORT")

if not mongodbHOST:
	mongodbHOST = "localhost"
if not mongodbPORT:
	mongodbPORT = "27017"
	
print("MONGO_PORT_27017_TCP_ADDR", mongodbHOST)
print("MONGO_PORT_27017_TCP_PORT", mongodbPORT)
		
# conectar a bd
#client = MongoClient("mongodb://localhost:27017");
client = MongoClient("mongodb://" + mongodbHOST + ":" + mongodbPORT)
db=client.descargadordb

#url = "http://www.ciens.ucv.ve/postgrado/"
#url = "http://learnmongo.com/posts/getting-started-with-mongodb-gridfs/"
#traer url de bd
#urlDoc=db.configs.find_one({"_id": 1})
#url = urlDoc['url_process']
url = sys.argv[1]
#print(url)

startDate = datetime.datetime.utcnow()

#serverStatusResult=db.command("serverStatus")
#pprint(serverStatusResult)

try:
	# buscar html dom
	#driver = webdriver.Firefox()
	options = webdriver.ChromeOptions()
	options.add_argument('headless')
	driver = webdriver.Chrome(options=options)
	#driver = webdriver.PhantomJS()
	driver.get(url)

	#revisar si hay errores en chrome
	errorDOM = driver.find_elements_by_class_name("error-code")
	# otros url (para retornar)
	urlsDOM = driver.find_elements_by_tag_name("a")
	# PARA BAJAR:
	# external style sheet
	linksDOM = driver.find_elements_by_tag_name("link")
	# imagenes
	imgsDOM = driver.find_elements_by_tag_name("img")
	# applets de java
	appletsDOM = driver.find_elements_by_tag_name("applet")
	# audio, video, picture
	sourcesDOM = driver.find_elements_by_tag_name("source")
	# flash 
	embedsDOM = driver.find_elements_by_tag_name("embed")
	#frames
	framesDOM = driver.find_elements_by_tag_name("frame")
	iframesDOM = driver.find_elements_by_tag_name("iframe")
	#embeded object
	objectsDOM = driver.find_elements_by_tag_name("object")
	#script
	scriptsDOM = driver.find_elements_by_tag_name("script")

	htmlDOM = driver.find_element_by_tag_name("html")
	#no sirve page_source, entrega cosas con ampersans
	htmlClean = driver.page_source

	statusBrowser = ""
	textDOM = ""
	#htmlClean = ""
	urls = []
	links = []
	imgs = []
	applets = []
	sources = []
	sourcesSet = []
	applets = []
	embeds = []
	frames = []
	iframes = []
	objects = []
	scripts = []
	# status= ['stored', 'notStored']
	status = ''
	if len(errorDOM) > 0:
		status = 'notStored'
		statusBrowser = errorDOM[0].get_attribute('textContent')
	else:
		try:
			status = 'stored'
			statusBrowser = "OK"
			#textDOM = htmlDOM.get_attribute('innerHTML') no uar innerHTML, por que elimina el tag html
			textDOM = htmlDOM.get_attribute('outerHTML')

			#LIMPIAR Y GUARDAR COMO URLS
			#print "URLS"
			for a in urlsDOM:
				if a.get_attribute('href'):
					urls.append(a.get_attribute('href'))
			#pprint(urls)

			#BUSCAR Y GARDAR COMO DEPENDENCIAS DEL HTML, SON PARTE DE EL
			#print "LINKS"
			for link in linksDOM:
				if link.get_attribute('href'):
					links.append(link.get_attribute('href'))

			#pprint(links)

			#print "IMG"
			for img in imgsDOM:
				if img.get_attribute('src'):
					imgs.append(img.get_attribute('src'))
			#pprint(imgs)

			#print "APPLET"
			for applet in appletsDOM:
				if applet.get_attribute('code'):
					applets.append(applet.get_attribute('code'))
			#pprint(applets)

			#print "SOURCE audio video"
			for source in sourcesDOM:
				if source.get_attribute('src'):
					sources.append(source.get_attribute('src'))
			#pprint(sources)

			#print "SOURCE SET picture"
			for source in sourcesDOM:
				if source.get_attribute('srcset'):
					sourcesSet.append(source.get_attribute('srcset'))
			#pprint(sourcesSet)

			#print "APPLET"
			for applet in appletsDOM:
				if applet.get_attribute('code'):
					applets.append(applet.get_attribute('code'))
			#pprint(applets)

			#print "EMBED"
			for embed in embedsDOM:
				if embed.get_attribute('src'):
					embeds.append(embed.get_attribute('src'))
			#pprint(embeds)

			#print "FRAME"
			for frame in framesDOM:
				if frame.get_attribute('src'):
					frames.append(frame.get_attribute('src'))
			#pprint(frames)

			#print "IFRAME"
			for iframe in iframesDOM:
				if iframe.get_attribute('src'):
					iframes.append(iframe.get_attribute('src'))
			#pprint(iframes)

			#print "OBJETC"
			for objecta in objectsDOM:
				if objecta.get_attribute('data'):
					objects.append(objecta.get_attribute('data'))
			#pprint(objects)

			#print "SCRIPTS"
			for script in scriptsDOM:
				if script.get_attribute('src'):
					#print(script.get_attribute('src'))
					scripts.append(script.get_attribute('src'))
		except:
			pass
	#pprint(scripts)
	#print(textDOM)
	#pprint(htmlDOM)
	#print(outerDOM)
	#print(htmlClean)

	driver.close()
	endDate = datetime.datetime.utcnow()

	#guardar en BD, crear el Json de guardar
	page = { 'url': url,
	  		 'created_date': startDate,
	  		 'downdload_date': endDate,
	  		 'status': status,
	  		 'status_browser': statusBrowser,
	  		 'url_related': urls,
			 'page_source': htmlClean,
	  		 'text_dom': textDOM,
			 'dependencias': {
					'external style sheet': links,
					'imagenes': imgs,
					'applets de java': applets,
					'audio, video': sources,
					'pictures': sourcesSet,
					'embeds flash': embeds,
					'frames': frames,
					'iframes': iframes,
					'embeded object': objects,
					'scripts': scripts
			  }
	  	   }

	#pprint(page)
	#print(url)

	#insertar
	result=db.pages.insert_one(page)
	#Print to the console the ObjectID of the new document
	print('Creada una pagina con id={0}'.format(result.inserted_id))
	#modificar status en config
	db.configs.update({"_id": 1},{"$set": {'status_process':'ready'}}, upsert=False)
except:
	e = sys.exc_info()[0]
	print('ERROR Selenium problemas de conexion: ', e )
	db.configs.update({"_id": 1},{"$set": {'status_process':'free'}}, upsert=False)	
